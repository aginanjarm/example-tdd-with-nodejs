'use strict'

const User = require('../src/User')

const config = require('./test_config'),
    chaiAsPromised = require('chai-as-promised')

config.chai.use(chaiAsPromised)


describe('User module', () => {  
	describe('feeling', () => {
		it('should return a Promise', () => {
            let promise = User.feeling
            return promise().then((result) => {
                config.expect(result).to.equal("Happy")
            })
		})
	})
})