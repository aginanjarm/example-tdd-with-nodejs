'use strict'

const test = require('../test_config'),
   	should = test.chai.should(),

    database = require('../../src/model/database')


database.init(test.database)

describe('Testing database ', () => {
    it('database run query', () => {
        // Without chai-as-promised
        // return database.query('SELECT 5 as value')
        //     .then((result) => {
        //         test1.expect(result.length).to.equal(1)
        //     })
        
        // With chai-as-promised
        return test.expect(
                database.query('SELECT 5 as value')
            ).to.eventually.deep.equal([ { value: 5 } ])
    })
})