'use strict'

const mocha = require('mocha'),
	describe = mocha.describe,
	it = mocha.it,

	chai = require('chai'),
	expect = chai.expect,
	should = chai.should()

module.exports = {
	'mocha'     :mocha, 
	'describe'  :describe, 
	'it'        :it, 
	'chai'      :chai, 
	'expect'    :expect, 
	'should'    :should,
	'database'	: {
		host: 'localhost',
		user: 'root',
		password: '',
		database: 'test_nodejs'
	}
}