# README #

The aim of this repo is for storing my exercise on implementing TDD with Node JS

## Environment and packages ##

* Node JS : v4.2.1
* npm 	: 3.10.8
* express
* mocha
* chai	
* eslint	: v3.19.0

For further details you can check it at _package.json_

## Installation ##
1. clone this repo
2. npm install
3. npm test

... to be continued


### Reference(s) ###
https://www.youtube.com/playlist?list=PLF5ApzZV2CSVBWlaX2d9rNUrBkg08pipy