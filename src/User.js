'use strict'

function feeling() {
	let status = new Promise((resolve, reject) => {
        let happy = true

        if(happy)
            resolve('Happy')
        
        reject('Unhappy')
    })

    return status
}

module.exports = {
	feeling
}