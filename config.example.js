module.exports = {
    // Site
    port    : 30001,
    static_folder : 'public',
    
    // Database configuration
    host : 'localhost',
    user : 'root',
    password : ''
    database : 'sample_db'
}