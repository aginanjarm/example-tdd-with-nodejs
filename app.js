const express = require('express'),
    app = express(),
    config = require('./config')

app.use(express.static(config.static_folder))

app.listen(config.port, ()=>{
    console.log("Express running ...")
})
